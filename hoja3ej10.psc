Algoritmo h3ejercicio10
	Escribir 'introduce la nota'
	Leer nota
	Si nota>0 Y nota<5 Entonces
		Escribir 'suspenso'
	SiNo
		Si nota>=5 Y nota<6 Entonces
			Escribir 'suficiente'
		SiNo
			Si nota>=6 Y nota<7 Entonces
				Escribir 'bien'
			SiNo
				Si nota>=7 Y nota<9 Entonces
					Escribir 'notable'
				SiNo
					Si nota>=9 Y nota<=10 Entonces
						Escribir 'sobresaliente'
					SiNo
						Escribir 'nota fuera de rango (0-10)'
					FinSi
				FinSi
			FinSi
		FinSi
	FinSi
FinAlgoritmo

